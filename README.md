# docker-monero

### Images

* [`monero`](monero): Monero P2P daemon and wallet RPC server
* [`wownero`](wownero): Wownero P2P daemon and wallet RPC server
* [`xmrig`](xmrig): XMRig miner
* [`xmrig-proxy`](xmrig-proxy): High performance Monero mining proxy
