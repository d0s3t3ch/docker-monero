# wownero

Docker image for the [Wownero][wownero-site] P2P daemon and wallet RPC server.

### Features

* Super minimal image. No BS.

### Daemon Usage Example

```
docker volume create wownerod_data
docker run \
       --name wownerod \
       --rm \
       -d \
       --entrypoint /bin/wownerod \
       -v wownerod_data:/data \
       -p 18080:18080 \
       -p 18081:18081 \
       moneromint/wownero \
       --non-interactive \
       --data-dir /data \
       --rpc-bind-ip 0.0.0.0 \
       --confirm-external-bind \
       --restricted-rpc
```

### Wallet Usage Example

```
docker run \
       --name monero-wallet-rpc \
       --rm \
       -d \
       --entrypoint /bin/monero-wallet-rpc \
       -v /var/lib/monero/wallet:/data \
       -p 18090:18090 \
       moneromint/monero \
       --daemon-host 1.2.3.4 \
       --trusted-daemon \
       --wallet-file /data/wallet \
       --shared-ringdb-dir /data/shared-ringdb \
       --rpc-bind-ip 0.0.0.0 \
       --rpc-bind-port 18090 \
       --confirm-external-bind \
       --disable-rpc-auth
```

### Links

* [Docker Hub][dockerhub]
* [git.wownero.com][gitwow]
* [GitHub][github]

[dockerhub]: https://hub.docker.com/r/moneromint/monero 'Docker Hub page'
[github]: https://github.com/moneormint/docker-monero 'GitHub repository'
[gitwow]: https://git.wownero.com/asymptotically/docker-monero 'Wownero Git repository'
[wownero-site]: https://wownero.org 'Wownero Site'
