# xmrig-proxy

Docker image for the [XMRig mining proxy][xmrig-proxy-gh].

### Features

* TLS with OpenSSL.
* Donation level defaults to zero. [Please send Mr XMRig some coins if you can][xmrig-donations].
* Single static binary. No BS.

### Usage Example

```
docker run \
    --rm \
    -d \
    --name xmrig-proxy \
    -p 3333:3333 \
    moneromint/xmrig-proxy \
    --bind=0.0.0.0:3333 \
    --url=xmrpow.de:4242 \
    --user=YOUR_WALLET_ADDRESS
```

### Links

* [Docker Hub][dockerhub]
* [git.wownero.com][gitwow]
* [GitHub][github]

[dockerhub]: https://hub.docker.com/r/moneromint/xmrig 'Docker Hub page'
[github]: https://github.com/moneromint/docker-monero 'GitHub repository'
[gitwow]: https://git.wownero.com/asymptotically/docker-monero 'Wownero Git repository'
[xmrig-proxy-gh]: https://github.com/xmrig/xmrig-proxy 'XMRig Proxy GitHub page'
[xmrig-donations]: https://github.com/xmrig/xmrig#donations 'XMRig donation addresses'
