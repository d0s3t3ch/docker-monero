# xmrig

Docker image for the [XMRig Monero miner][xmrig-gh].

### Features

* TLS with OpenSSL.
* Donation level defaults to zero. [Please send Mr XMRig some coins if you can][xmrig-donations].
* Single static binary. No BS.

### Usage Example

```
docker run \
    --rm \
    -i \
    -t \
    moneromint/xmrig \
    -a rx/0 \
    -o xmrpow.de:4242 \
    -u YOUR_WALLET_ADDRESS \
```

### Performance Notes

Enable huge pages and apply the MSR tweaks from the host machine; the container is unable to do this.

### Links

* [Docker Hub][dockerhub]
* [git.wownero.com][gitwow]
* [GitHub][github]

[dockerhub]: https://hub.docker.com/r/moneromint/xmrig 'Docker Hub page'
[github]: https://github.com/moneormint/docker-monero 'GitHub repository'
[gitwow]: https://git.wownero.com/asymptotically/docker-monero 'Wownero Git repository'
[xmrig-gh]: https://github.com/xmrig/xmrig 'XMRig GitHub page'
[xmrig-donations]: https://github.com/xmrig/xmrig#donations 'XMRig donation addresses'
